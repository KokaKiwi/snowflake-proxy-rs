use std::fmt;

use miette::{Diagnostic, ReportHandler};
use thiserror::Error;

#[derive(Debug, Error, Diagnostic)]
pub enum Error {
    #[error(transparent)]
    #[diagnostic(code(snowflake_proxy::io_error))]
    Io(#[from] std::io::Error),

    #[error(transparent)]
    #[diagnostic(code(snowflake_proxy::webrtc))]
    WebRtc(#[from] webrtc::Error),

    #[error(transparent)]
    #[diagnostic(code(snowflake_proxy::webrtc::ice))]
    WebRtcIce(#[from] webrtc::ice::Error),

    #[error(transparent)]
    #[diagnostic(code(snowflake_proxy::http))]
    Http(#[from] reqwest::Error),

    #[error(transparent)]
    #[diagnostic(code(snowflake_proxy::parse_url))]
    ParseUrl(#[from] url::ParseError),

    #[error(transparent)]
    #[diagnostic(code(snowflake_proxy::websocket))]
    WebSocket(#[from] tokio_tungstenite::tungstenite::Error),

    #[error(transparent)]
    Shutdown(#[from] tokio_graceful_shutdown::errors::GracefulShutdownError<Error>),

    #[error("Client timed out")]
    #[diagnostic(code(snowflake_proxy::client_timeout))]
    ClientTimeout,

    #[error("{0}")]
    #[diagnostic(code(snowflake_proxy::custom))]
    Custom(String),
}

impl Error {
    pub fn custom(msg: impl fmt::Display) -> Error {
        Error::Custom(msg.to_string())
    }

    pub fn report(&self) -> Reported {
        Reported(self)
    }
}

pub struct Reported<'a>(&'a Error);

impl<'a> Reported<'a> {
    fn handler() -> impl ReportHandler {
        miette::MietteHandler::new()
    }
}

impl<'a> fmt::Display for Reported<'a> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        Reported::handler().display(self.0, f)
    }
}

impl<'a> fmt::Debug for Reported<'a> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        Reported::handler().debug(self.0, f)
    }
}

pub type Result<T, E = Error> = std::result::Result<T, E>;
