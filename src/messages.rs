use serde::{Deserialize, Serialize};
use webrtc::peer_connection::sdp::session_description::RTCSessionDescription;

use crate::proxy::nat;

#[derive(Debug, Clone, Copy, PartialEq, Eq, Deserialize, Serialize)]
#[serde(rename_all = "lowercase")]
pub enum ProxyType {
    Standalone,
    WebExt,
    Badge,
    IPTProxy,
}

#[derive(Debug, Clone, Deserialize, Serialize)]
#[serde(rename_all = "PascalCase")]
pub struct ProxyPollRequest {
    pub sid: String,
    pub version: String,
    #[serde(rename = "Type")]
    pub proxy_type: ProxyType,
    #[serde(rename = "NAT")]
    pub nat_type: nat::Type,
    pub clients: u32,
}

#[allow(clippy::large_enum_variant)]
#[serde_with::serde_as]
#[derive(Debug, Clone, Deserialize, Serialize)]
#[serde(tag = "Status")]
pub enum ProxyPollResponse {
    #[serde(rename = "client match", rename_all = "PascalCase")]
    ClientMatch {
        #[serde_as(as = "serde_with::json::JsonString")]
        offer: RTCSessionDescription,
        #[serde(rename = "NAT")]
        nat_type: nat::Type,
    },
    #[serde(rename = "no match")]
    NoMatch,
}

#[serde_with::serde_as]
#[derive(Debug, Clone, Deserialize, Serialize)]
#[serde(rename_all = "PascalCase")]
pub struct ProxyAnswerRequest {
    pub version: String,
    pub sid: String,
    #[serde_as(as = "serde_with::json::JsonString")]
    pub answer: RTCSessionDescription,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Deserialize, Serialize)]
#[serde(tag = "Status")]
pub enum ProxyAnswerResponse {
    #[serde(rename = "success")]
    Success,
    #[serde(rename = "client gone")]
    ClientGone,
}

#[allow(dead_code)]
impl ProxyAnswerResponse {
    pub fn is_success(self) -> bool {
        self == ProxyAnswerResponse::Success
    }

    pub fn is_client_gone(self) -> bool {
        self == ProxyAnswerResponse::ClientGone
    }
}
