#![forbid(unsafe_code)]

use clap::Parser;

use cli::Options;
pub(crate) use errors::{Error, Result};

#[macro_use]
mod util;

shadow_rs::shadow!(build);

mod cli;
mod errors;
mod messages;
mod proxy;

fn main() -> miette::Result<()> {
    let opts = Options::parse();
    setup_logging(&opts);

    tokio::runtime::Builder::new_multi_thread()
        .enable_all()
        .thread_name("snowflake-proxy-worker")
        .build()
        .map_err(Error::from)?
        .block_on(proxy::run(opts.into()))
        .map_err(Error::into)
}

fn setup_logging(_opts: &Options) {
    use tracing_error::ErrorLayer;
    use tracing_subscriber::fmt::time as tracing_time;
    use tracing_subscriber::{prelude::*, EnvFilter};

    let fmt_layer = tracing_subscriber::fmt::layer()
        .with_thread_names(true)
        .with_timer(tracing_time::UtcTime::new(
            time::macros::format_description!(
                "[year]-[month]-[day] [hour]:[minute]:[second]+[offset_hour]"
            ),
        ))
        .with_filter(EnvFilter::from_default_env());

    let registry = tracing_subscriber::registry()
        .with(fmt_layer)
        .with(ErrorLayer::default());

    #[cfg(feature = "debug")]
    let registry = registry.with(console_subscriber::spawn());

    registry.init();
}
