use clap::Parser;
use reqwest::Url;
use webrtc::ice::url::Url as IceUrl;

use crate::build;
use crate::util::cli as cli_util;

const CLAP_LONG_VERSION: &str = shadow_rs::formatcp!(
    r#"{version}
branch: {branch}
commit: {commit_hash} ({commit_date})
build time: {build_time}
build env: {rust_version} ({rust_channel})"#,
    version = build::PKG_VERSION,
    branch = build::BRANCH,
    commit_hash = build::SHORT_COMMIT,
    commit_date = build::COMMIT_DATE,
    build_time = build::BUILD_TIME,
    rust_version = build::RUST_VERSION,
    rust_channel = build::RUST_CHANNEL,
);

#[derive(Debug, Parser)]
#[clap(
    version, author, about,
    long_version = CLAP_LONG_VERSION,
    color = concolor_clap::color_choice()
)]
pub struct Options {
    #[clap(flatten)]
    pub endpoints: Endpoints,

    #[clap(long, env)]
    pub keep_local_addresses: bool,

    #[clap(long, env, value_parser = cli_util::ports_range_parser)]
    pub ephemeral_ports_range: Option<(u16, u16)>,
}

const DEFAULT_BROKER_URL: &str = "https://snowflake-broker.torproject.net";
const DEFAULT_NAT_PROBE_URL: &str = "https://snowflake-broker.freehaven.net:8443/probe";
const DEFAULT_RELAY_URL: &str = "wss://snowflake.bamsoftware.com";
const DEFAULT_ICE_SERVERS: &[&str] = &["stun:stun.stunprotocol.org:3478"];

#[derive(Debug, Parser)]
pub struct Endpoints {
    #[clap(long, env, default_value = DEFAULT_BROKER_URL)]
    pub broker_url: Url,

    #[clap(long, env, default_value = DEFAULT_NAT_PROBE_URL)]
    pub nat_probe_url: Url,

    #[clap(long, env, default_value = DEFAULT_RELAY_URL)]
    pub relay_url: Url,

    #[clap(
        long, env,
        value_parser = cli_util::ValidatorParser::new(IceUrl::parse_url),
        use_value_delimiter = true,
        default_values = DEFAULT_ICE_SERVERS
    )]
    pub ice_servers: Vec<String>,
}
