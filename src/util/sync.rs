use std::sync::{Arc, Mutex};

use tokio::sync::Mutex as AsyncMutex;

pub type Shared<T> = Arc<Mutex<T>>;

pub fn share<T>(value: T) -> Shared<T> {
    Arc::new(Mutex::new(value))
}

pub type AsyncShared<T> = Arc<AsyncMutex<T>>;

pub fn async_share<T>(value: T) -> AsyncShared<T> {
    Arc::new(AsyncMutex::new(value))
}
