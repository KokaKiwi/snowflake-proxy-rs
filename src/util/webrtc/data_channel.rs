use std::sync::Arc;

use futures::Stream;
use tokio::{io, sync::mpsc};
use webrtc::{data_channel, peer_connection};

use crate::util::sync;

#[async_trait::async_trait]
pub trait DataChannelHandler {
    type Error;

    async fn on_open(&mut self) -> Result<(), Self::Error> {
        Ok(())
    }
    async fn on_close(&mut self) -> Result<(), Self::Error> {
        Ok(())
    }

    async fn on_error(&mut self, _err: webrtc::Error) -> Result<(), Self::Error> {
        Ok(())
    }

    async fn on_message(
        &mut self,
        _msg: data_channel::data_channel_message::DataChannelMessage,
    ) -> Result<(), Self::Error> {
        Ok(())
    }
}

pub async fn register_data_channel_handler<H>(
    data_channel: Arc<data_channel::RTCDataChannel>,
    handler: H,
) where
    H: DataChannelHandler + Send + Sync + 'static,
    H::Error: std::fmt::Display,
{
    let handler = sync::async_share(handler);

    data_channel
        .on_open(cloned!(
            handler: boxed_async_fn!(handler: || {
                tracing::trace!(event = "Open", "webrtc.data_channel");

                let mut handler = handler.lock().await;
                log_error!(handler.on_open().await);
            })
        ))
        .await;
    data_channel
        .on_close(cloned!(
            handler: boxed_async_fn!(handler: || {
                tracing::trace!(event = "Close", "webrtc.data_channel");

                let mut handler = handler.lock().await;
                log_error!(handler.on_close().await);
            })
        ))
        .await;
    data_channel
        .on_message(cloned!(
            handler: boxed_async_fn!(handler: |msg| {
                tracing::trace!(event = "Message", msg = ?msg, "webrtc.data_channel");

                let mut handler = handler.lock().await;
                log_error!(handler.on_message(msg).await);
            })
        ))
        .await;
    data_channel
        .on_error(cloned!(
            handler: boxed_async_fn!(handler: |err| {
                tracing::trace!(event = "Error", err = ?err, "webrtc.data_channel");

                let mut handler = handler.lock().await;
                log_error!(handler.on_error(err).await);
            })
        ))
        .await;
}

#[derive(Debug)]
pub enum DataChannelEvent {
    Open,
    Close,
    Message(data_channel::data_channel_message::DataChannelMessage),
    Error(webrtc::Error),
}

pub async fn make_data_channel_receiver(
    data_channel: Arc<data_channel::RTCDataChannel>,
) -> mpsc::Receiver<DataChannelEvent> {
    let (tx, rx) = mpsc::channel(64);

    struct ReceiverHandler(mpsc::Sender<DataChannelEvent>);

    #[async_trait::async_trait]
    impl DataChannelHandler for ReceiverHandler {
        type Error = Box<dyn std::error::Error + 'static>;

        async fn on_open(&mut self) -> Result<(), Self::Error> {
            Ok(self.0.send(DataChannelEvent::Open).await?)
        }

        async fn on_close(&mut self) -> Result<(), Self::Error> {
            Ok(self.0.send(DataChannelEvent::Close).await?)
        }

        async fn on_message(
            &mut self,
            msg: data_channel::data_channel_message::DataChannelMessage,
        ) -> Result<(), Self::Error> {
            Ok(self.0.send(DataChannelEvent::Message(msg)).await?)
        }

        async fn on_error(&mut self, err: webrtc::Error) -> Result<(), Self::Error> {
            Ok(self.0.send(DataChannelEvent::Error(err)).await?)
        }
    }

    register_data_channel_handler(data_channel, ReceiverHandler(tx)).await;

    rx
}

pub async fn make_data_channel_stream(
    data_channel: Arc<data_channel::RTCDataChannel>,
) -> impl Stream<Item = DataChannelEvent> {
    use tokio_stream::wrappers::ReceiverStream;

    ReceiverStream::new(make_data_channel_receiver(data_channel).await)
}

pub async fn make_data_channel_io_stream(
    peer_connection: Arc<peer_connection::RTCPeerConnection>,
    data_channel: Arc<data_channel::RTCDataChannel>,
) -> io::DuplexStream {
    const BUFFER_SIZE: usize = 512;

    let (tx, rx) = io::duplex(BUFFER_SIZE);

    let (reader, writer) = io::split(tx);

    tokio::spawn(cloned!(data_channel: async move {
        use bytes::Bytes;
        use tokio::io::AsyncReadExt;

        let mut buf = [0; BUFFER_SIZE];
        let mut reader = reader;

        loop {
            use webrtc::peer_connection::peer_connection_state::RTCPeerConnectionState;

            if matches!(peer_connection.connection_state(), RTCPeerConnectionState::Disconnected) {
                break;
            }

            let read = reader.read(&mut buf).await.unwrap();
            if read == 0 {
                break;
            }

            let data = Bytes::copy_from_slice(&buf[..read]);
            if data_channel.send(&data).await.is_err() {
                break;
            }
        }

        log_error!(peer_connection.close().await);
    }));

    let writer = sync::async_share(writer);
    data_channel
        .on_message(cloned!(
            writer: boxed_async_fn!(writer: |msg| {
                use tokio::io::AsyncWriteExt;

                let mut writer = writer.lock().await;
                log_error!(writer.write_all(&msg.data).await);
            })
        ))
        .await;
    data_channel
        .on_close(cloned!(
            writer: boxed_async_fn!(writer: || {
                use tokio::io::AsyncWriteExt;

                let mut writer = writer.lock().await;
                log_error!(writer.shutdown().await);
            })
        ))
        .await;
    data_channel
        .on_error(cloned!(
            writer: boxed_async_fn!(writer: |err| {
                use tokio::io::AsyncWriteExt;

                tracing::warn!(?err, "DataChannel::on_error");

                let mut writer = writer.lock().await;
                log_error!(writer.shutdown().await);
            })
        ))
        .await;

    rx
}
