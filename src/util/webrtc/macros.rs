macro_rules! boxed_async_fn {
    ($($($cap:ident),+:)? |$($arg_name:ident),*| $body:block) => {
        Box::new(move |$($arg_name),*| {
            $($(
                #[allow(unused_mut)]
                let mut $cap = $cap.clone();
            )+)?
            Box::pin(async move {
                $body
            })
        })
    };
    ($($($cap:ident),+:)? || $body:block) => {
        boxed_async_fn!($($($cap),+:)? | | $body)
    };
}
