macro_rules! cloned {
    ($($name:ident),+: $expr:expr) => {
        {
            $(
                #[allow(unused_mut)]
                let mut $name = $name.clone();
            )+
            $expr
        }
    }
}

macro_rules! subsystem {
    (|$handle_name:ident : Handle<$err_type:ty>| $subsystem:expr) => {
        |handle: tokio_graceful_shutdown::SubsystemHandle<$err_type>| async move {
            let $handle_name = handle.clone();

            tokio::select! {
                _ = handle.on_shutdown_requested() => Ok(()),
                res = $subsystem => res,
            }
        }
    };
    (|$handle_name:ident| $subsystem:expr) => {
        |handle: tokio_graceful_shutdown::SubsystemHandle| async move {
            let $handle_name = handle.clone();

            tokio::select! {
                _ = handle.on_shutdown_requested() => Ok(()),
                res = $subsystem => res,
            }
        }
    };
}

macro_rules! log_error {
    ($expr:expr) => {
        match $expr {
            Ok(_) => {}
            Err(err) => tracing::error!("{err}"),
        }
    };

    ($expr:expr, $msg:expr) => {
        match $expr {
            Ok(_) => {}
            Err(err) => tracing::error!("{}: {err}", $msg),
        }
    };
}

macro_rules! catch {
    // Sync
    {
        try $try:block
        catch $err_name:ident : $err_type:ty $catch:block
    } => {{
        let closure = || Ok::<_, $err_type>($try);
        match closure() {
            Ok(value) => value,
            Err($err_name) => $catch,
        }
    }};
    // Boxed catch
    {
        try $try:block
        catch dyn $err_name:ident $catch:block
    } => {
        catch! {
            try $try
            catch $err_name: Box<dyn std::error::Error + 'static> $catch
        }
    };
    {
        try $try:block
        catch dyn $catch:block
    } => {
        catch! {
            try $try
            catch dyn _err $catch
        }
    };
    // Inferred catch
    {
        try $try:block
        catch $err_name:ident $catch:block
    } => {
        catch! {
            try $try
            catch $err_name: _ $catch
        }
    };
    {
        try $try:block
        catch $catch:block
    } => {
        catch! {
            try $try
            catch _err $catch
        }
    };

    // Async
    {
        async try $try:block
        catch $err_name:ident : $err_type:ty $catch:block
    } => {{
        let closure = || async { Ok::<_, $err_type>($try) };
        match closure().await {
            Ok(value) => value,
            Err($err_name) => $catch,
        }
    }};
    // Boxed catch
    {
        async try $try:block
        catch dyn $err_name:ident $catch:block
    } => {
        catch! {
            async try $try
            catch $err_name: Box<dyn std::error::Error + 'static> $catch
        }
    };
    {
        async try $try:block
        catch dyn $catch:block
    } => {
        catch! {
            async try $try
            catch dyn _err $catch
        }
    };
    // Inferred catch
    {
        async try $try:block
        catch $err_name:ident $catch:block
    } => {
        catch! {
            async try $try
            catch $err_name: _ $catch
        }
    };
    {
        async try $try:block
        catch $catch:block
    } => {
        catch! {
            async try $try
            catch _err $catch
        }
    };
}
