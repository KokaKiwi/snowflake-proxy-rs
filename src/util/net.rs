use std::net::{IpAddr, Ipv4Addr, Ipv6Addr};

#[easy_ext::ext(IpAddrExt)]
pub impl IpAddr {
    fn is_local(&self) -> bool {
        match self {
            IpAddr::V4(addr) => addr.is_local(),
            IpAddr::V6(addr) => addr.is_local(),
        }
    }
}

impl IpAddrExt for Ipv4Addr {
    fn is_local(&self) -> bool {
        self.is_loopback() || self.is_private() || self.is_link_local()
    }
}

impl IpAddrExt for Ipv6Addr {
    fn is_local(&self) -> bool {
        self.is_loopback() || self.is_unique_local_stable() || self.is_unicast_stable()
    }
}

#[easy_ext::ext]
impl Ipv6Addr {
    fn is_unique_local_stable(&self) -> bool {
        (self.segments()[0] & 0xfe00) == 0xfc00
    }

    fn is_unicast_stable(&self) -> bool {
        !self.is_multicast()
    }
}
