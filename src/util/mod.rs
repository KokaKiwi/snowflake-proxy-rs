#[macro_use]
mod macros;
#[macro_use]
pub mod webrtc;

pub mod cli;
pub mod net;
pub mod sync;
pub mod websocket;

pub fn random_string(alphabet: &[u8], len: usize) -> String {
    use rand::distributions::Slice;
    use rand::Rng;

    debug_assert_ne!(alphabet.len(), 0);

    let dist = Slice::new(alphabet).unwrap();
    let rng = rand::thread_rng();

    let bytes: Vec<u8> = rng.sample_iter(&dist).take(len).cloned().collect();

    String::from_utf8(bytes).unwrap()
}

pub fn random_id(len: usize) -> String {
    static URL_SAFE_BASE64_ALPHABE: &[u8] =
        b"ModuleSymbhasOwnPr-0123456789ABCDEFGHNRVfgctiUvz_KqYTJkLxpZXIjQW";

    random_string(URL_SAFE_BASE64_ALPHABE, len)
}
