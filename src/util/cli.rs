use clap::builder::TypedValueParser;

use crate::errors::{Error, Result};

#[derive(Clone, Debug)]
pub struct ValidatorParser<V: Validator>(V);

impl<V: Validator> ValidatorParser<V> {
    pub fn new(validator: V) -> Self {
        Self(validator)
    }
}

impl<V: Validator> TypedValueParser for ValidatorParser<V> {
    type Value = String;

    fn parse_ref(
        &self,
        cmd: &clap::Command,
        arg: Option<&clap::Arg>,
        value: &std::ffi::OsStr,
    ) -> Result<Self::Value, clap::Error> {
        use clap::builder::StringValueParser;

        let value = StringValueParser::new().parse_ref(cmd, arg, value)?;
        self.0
            .validate(&value)
            .map_err(|err| clap::Error::raw(clap::error::ErrorKind::ValueValidation, err))?;
        Ok(value)
    }
}

pub trait Validator: Clone + Send + Sync + 'static {
    type Error: std::error::Error + Send + Sync + 'static;

    fn validate(&self, value: &str) -> Result<(), Self::Error>;
}

impl<F, T, E> Validator for F
where
    F: Fn(&str) -> Result<T, E> + Clone + Send + Sync + 'static,
    T: Send + Sync + Clone,
    E: std::error::Error + Send + Sync + 'static,
{
    type Error = E;

    fn validate(&self, value: &str) -> Result<(), Self::Error> {
        (*self)(value).map(|_| ())
    }
}

pub fn ports_range_parser(value: &str) -> Result<(u16, u16)> {
    if let Some((left, right)) = value.split_once(':') {
        let port_min = left.parse().map_err(Error::custom)?;
        let port_max = right.parse().map_err(Error::custom)?;

        if port_max < port_min {
            return Err(Error::custom("Max port must be higher than min port"));
        }

        Ok((port_min, port_max))
    } else {
        Err(Error::custom("Bad port range"))
    }
}
