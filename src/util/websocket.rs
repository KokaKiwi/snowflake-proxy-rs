use std::pin::Pin;
use std::task::{self, Poll};

use tokio::io;
use tokio_tungstenite::WebSocketStream;

#[pin_project::pin_project]
pub struct WebSocketConnection<S> {
    #[pin]
    inner: WebSocketStream<S>,
    read_state: ReadState,
}

#[derive(Debug)]
enum ReadState {
    Ready(bytes::Bytes),
    Pending,
    Eof,
}

impl<S> WebSocketConnection<S> {
    pub fn new(inner: WebSocketStream<S>) -> Self {
        WebSocketConnection {
            inner,
            read_state: ReadState::Pending,
        }
    }
}

impl<S> io::AsyncRead for WebSocketConnection<S>
where
    S: io::AsyncRead + io::AsyncWrite + Unpin,
{
    fn poll_read(
        self: Pin<&mut Self>,
        cx: &mut task::Context<'_>,
        buf: &mut io::ReadBuf<'_>,
    ) -> Poll<std::io::Result<()>> {
        use bytes::Buf;
        use futures::Stream;

        let mut this = self.project();

        loop {
            match this.read_state {
                ReadState::Ready(data) => {
                    let length = std::cmp::min(buf.remaining(), data.len());
                    let dst = buf.initialize_unfilled_to(length);

                    data.copy_to_slice(dst);
                    buf.advance(length);

                    if data.is_empty() {
                        *this.read_state = ReadState::Pending;
                    }

                    return Poll::Ready(Ok(()));
                }
                ReadState::Pending => match futures::ready!(this.inner.as_mut().poll_next(cx)) {
                    Some(Ok(msg)) => {
                        *this.read_state = ReadState::Ready(msg.into_data().into());
                    }
                    Some(Err(err)) => {
                        use std::io::{Error, ErrorKind};

                        *this.read_state = ReadState::Eof;

                        return Poll::Ready(Err(Error::new(ErrorKind::Other, err)));
                    }
                    None => {
                        *this.read_state = ReadState::Eof;
                        return Poll::Ready(Ok(()));
                    }
                },
                ReadState::Eof => return Poll::Ready(Ok(())),
            }
        }
    }
}

impl<S> io::AsyncWrite for WebSocketConnection<S>
where
    S: io::AsyncRead + io::AsyncWrite + Unpin,
{
    fn poll_write(
        self: Pin<&mut Self>,
        cx: &mut task::Context<'_>,
        buf: &[u8],
    ) -> Poll<std::io::Result<usize>> {
        use futures::Sink;
        use tokio_tungstenite::tungstenite::Message;

        let mut this = self.project();

        futures::ready!(this.inner.as_mut().poll_ready(cx).map_err(into_io_error))?;

        let msg = Message::binary(buf);
        this.inner.start_send(msg).map_err(into_io_error)?;

        Poll::Ready(Ok(buf.len()))
    }

    fn poll_flush(self: Pin<&mut Self>, cx: &mut task::Context<'_>) -> Poll<std::io::Result<()>> {
        use futures::Sink;

        self.project().inner.poll_flush(cx).map_err(into_io_error)
    }

    fn poll_shutdown(
        self: Pin<&mut Self>,
        cx: &mut task::Context<'_>,
    ) -> Poll<std::io::Result<()>> {
        use futures::Sink;

        self.project().inner.poll_close(cx).map_err(into_io_error)
    }
}

fn into_io_error<E>(err: E) -> std::io::Error
where
    E: Into<Box<dyn std::error::Error + Send + Sync>>,
{
    use std::io::{Error, ErrorKind};

    Error::new(ErrorKind::Other, err)
}
