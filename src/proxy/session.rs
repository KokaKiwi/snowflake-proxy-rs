use std::time::Duration;

use tokio_graceful_shutdown::SubsystemHandle;
use tracing_futures::Instrument;

use super::SnowflakeProxy;
use crate::{Error, Result};

const POLL_INTERVAL: Duration = Duration::from_secs(5);
const DATA_CHANNEL_TIMEOUT: Duration = Duration::from_secs(20);

const SESSION_ID_LENGTH: usize = 16;

pub async fn connector(
    handle: SubsystemHandle<crate::errors::Error>,
    proxy: SnowflakeProxy,
) -> Result<()> {
    loop {
        let session_id = crate::util::random_id(SESSION_ID_LENGTH);

        let span = tracing::trace_span!("session", id = %session_id);
        run_session(&handle, &proxy, session_id)
            .instrument(span)
            .await?;

        tokio::time::sleep(POLL_INTERVAL).await;
    }
}

async fn run_session(
    _handle: &SubsystemHandle<crate::errors::Error>,
    proxy: &SnowflakeProxy,
    id: String,
) -> Result<()> {
    use crate::messages;

    tracing::debug!("Running session");

    // Poll offer
    let clients = {
        let guard = proxy.clients.lock().unwrap();
        *guard & (!0b111) // Round down to multiple of 8
    };
    let nat_type = {
        let guard = proxy.nat_type.lock().unwrap();
        *guard
    };

    let offer_url = proxy.config.endpoints.broker_url.join("proxy")?;
    let offer_payload = messages::ProxyPollRequest {
        version: "1.2".into(),
        sid: id.clone(),
        clients,
        nat_type,
        proxy_type: messages::ProxyType::Standalone,
    };

    let res = proxy
        .http_client
        .post(offer_url)
        .json(&offer_payload)
        .send()
        .await?
        .error_for_status()?;

    let offer = match res.json::<messages::ProxyPollResponse>().await? {
        messages::ProxyPollResponse::ClientMatch { offer, .. } => offer,
        messages::ProxyPollResponse::NoMatch => {
            tracing::debug!("No client match");
            return Ok(());
        }
    };
    tracing::debug!(?offer, "Received offer");

    // Create PeerConnection
    let peer_conn = proxy.make_webrtc_peer_connection().await?;
    let peer_conn = std::sync::Arc::new(peer_conn);

    catch! {
        async try {
            let client_token = proxy.make_client_token();

            peer_conn.set_remote_description(offer).await?;

            let answer = peer_conn.create_answer(None).await?;
            peer_conn.set_local_description(answer.clone()).await?;
            peer_conn.gathering_complete_promise().await.recv().await;

            let (ready_tx, mut ready_rx) = tokio::sync::mpsc::channel::<()>(1);

            let stream_span = tracing::trace_span!("stream");
            peer_conn.on_data_channel(cloned!(proxy, peer_conn, client_token, stream_span:
                boxed_async_fn!(proxy, peer_conn, client_token, ready_tx, stream_span: |data_channel| {
                    use crate::util::webrtc::data_channel::make_data_channel_io_stream;

                    let io_stream = make_data_channel_io_stream(peer_conn.clone(), data_channel.clone()).await;

                    catch! {
                        async try {
                            use tracing_futures::Instrument;

                            use crate::util::websocket::WebSocketConnection;

                            let (ws_stream, _) = tokio_tungstenite::connect_async(&proxy.config.endpoints.relay_url).await?;
                            let ws_stream = WebSocketConnection::new(ws_stream);

                            let client_token = client_token.clone();
                            tokio::spawn(async move {
                                let mut io_stream = io_stream;
                                let mut ws_stream = ws_stream;

                                if let Err(err) = tokio::io::copy_bidirectional(&mut io_stream, &mut ws_stream).await {
                                    tracing::debug!("Stream closed with error: {:?}", err);
                                }
                                tracing::debug!("Connection closed");
                                client_token.release();
                            }.instrument(stream_span));

                            log_error!(ready_tx.send(()).await);
                        } catch _err: crate::Error {
                            client_token.release();
                        }
                    }
                }))
            ).await;

            let answer_url = proxy.config.endpoints.broker_url.join("answer")?;
            let answer_payload = messages::ProxyAnswerRequest {
                version: "1.2".into(),
                sid: id.clone(),
                answer,
            };

            let res = proxy.http_client
                .post(answer_url)
                .json(&answer_payload)
                .send()
                .await?
                .error_for_status()?;

            let answer: messages::ProxyAnswerResponse = res.json().await?;
            if answer.is_client_gone() {
                return Err(Error::ClientTimeout);
            }

            if let Ok(Some(_)) = tokio::time::timeout(DATA_CHANNEL_TIMEOUT, ready_rx.recv()).await {
                client_token.acquire();
            } else {
                return Err(Error::ClientTimeout);
            }
        } catch err: crate::Error {
            peer_conn.close().await?;
            return Err(err);
        }
    }

    Ok(())
}
