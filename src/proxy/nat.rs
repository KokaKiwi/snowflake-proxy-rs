use std::time::Duration;

use serde::{Deserialize, Serialize};

use super::SnowflakeProxy;
use crate::Result;

const NAT_TYPE_UPDATE_INTERVAL: Duration = Duration::from_secs(60 * 60);

const DATA_CHANNEL_TIMEOUT: Duration = Duration::from_secs(20);

#[derive(Debug, Clone, Copy, PartialEq, Eq, Deserialize, Serialize, strum::Display)]
#[serde(rename_all = "lowercase")]
pub enum Type {
    Unknown,
    Restricted,
    Unrestricted,
}

pub async fn type_updater(proxy: SnowflakeProxy) -> Result<()> {
    loop {
        let previous = {
            let nat_type = proxy.nat_type.lock().unwrap();
            *nat_type
        };

        let next = check_nat_type(&proxy).await?;

        tracing::debug!(previous = %previous, next = %next, "nat.update_type");

        {
            let mut nat_type = proxy.nat_type.lock().unwrap();
            *nat_type = next;
        }

        tokio::time::sleep(NAT_TYPE_UPDATE_INTERVAL).await;
    }
}

pub async fn check_nat_type(proxy: &SnowflakeProxy) -> Result<Type> {
    use crate::messages;
    use crate::util::webrtc::data_channel;
    use futures::StreamExt;
    use tokio::time;

    tracing::debug!("Checking NAT type...");

    let peer_conn = proxy.make_webrtc_peer_connection().await?;

    let data_channel = peer_conn
        .create_data_channel("test", Some(Default::default()))
        .await?;
    let events = data_channel::make_data_channel_stream(data_channel).await;

    let sdp = peer_conn.create_offer(None).await?;

    peer_conn.set_local_description(sdp).await?;
    peer_conn.gathering_complete_promise().await.recv().await;

    let sdp = peer_conn
        .local_description()
        .await
        .expect("Should have a local connection description");
    tracing::debug!(offer = ?sdp, "Creating offer...");

    let poll_response = messages::ProxyPollResponse::ClientMatch {
        offer: sdp,
        nat_type: Type::Unknown,
    };

    let res = proxy
        .http_client
        .post(proxy.config.endpoints.nat_probe_url.clone())
        .json(&poll_response)
        .send()
        .await?;

    let answer: messages::ProxyAnswerRequest = res.error_for_status()?.json().await?;
    peer_conn.set_remote_description(answer.answer).await?;

    let mut completed = events.filter(|event| {
        futures::future::ready(matches!(event, data_channel::DataChannelEvent::Close))
    });

    let nat_type = match time::timeout(DATA_CHANNEL_TIMEOUT, completed.next()).await {
        Ok(_) => Type::Unrestricted,
        Err(_) => Type::Restricted,
    };

    peer_conn.close().await?;

    Ok(nat_type)
}
