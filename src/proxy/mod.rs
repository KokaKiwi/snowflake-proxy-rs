use reqwest::{Client, Url};
use webrtc::{
    api::API,
    peer_connection::{
        configuration::RTCConfiguration as WebRTCConfiguration,
        RTCPeerConnection as WebRTCPeerConnection,
    },
};

use crate::cli;
use crate::util::sync;
use crate::Result;

pub mod nat;
mod session;

const HTTP_USER_AGENT: &str = concat!("snowflake-proxy-rs/", env!("CARGO_PKG_VERSION"));

#[derive(Clone)]
pub struct SnowflakeProxy {
    pub config: Config,
    pub nat_type: sync::Shared<nat::Type>,
    pub clients: sync::Shared<u32>,
    pub http_client: Client,
}

impl SnowflakeProxy {
    pub fn make_webrtc_api(&self) -> Result<API> {
        let setting_engine = {
            use webrtc::api::setting_engine::SettingEngine;
            use webrtc::ice::mdns::MulticastDnsMode;

            let mut setting_engine = SettingEngine::default();

            if let Some((port_min, port_max)) = self.config.ephemeral_ports_range {
                use webrtc::ice::udp_network::{EphemeralUDP, UDPNetwork};

                let udp_network = UDPNetwork::Ephemeral(EphemeralUDP::new(port_min, port_max)?);
                setting_engine.set_udp_network(udp_network);
            }

            if !self.config.keep_local_addresses {
                use crate::util::net::IpAddrExt;

                setting_engine
                    .set_ip_filter(Box::new(|ip| !(ip.is_local() || ip.is_unspecified())));
            }

            setting_engine.set_ice_multicast_dns_mode(MulticastDnsMode::Disabled);

            setting_engine
        };

        let mut media_engine = {
            use webrtc::api::media_engine::MediaEngine;

            let mut media_engine: MediaEngine = MediaEngine::default();

            media_engine.register_default_codecs()?;

            media_engine
        };

        let interceptor_registry = {
            use webrtc::api::interceptor_registry::register_default_interceptors;
            use webrtc::interceptor::registry::Registry;

            let interceptor_registry = Registry::new();

            register_default_interceptors(interceptor_registry, &mut media_engine)?
        };

        let api = webrtc::api::APIBuilder::new()
            .with_setting_engine(setting_engine)
            .with_media_engine(media_engine)
            .with_interceptor_registry(interceptor_registry)
            .build();

        Ok(api)
    }

    pub async fn make_webrtc_peer_connection(&self) -> Result<WebRTCPeerConnection> {
        let api = self.make_webrtc_api()?;
        let peer_connection = api
            .new_peer_connection(self.config.make_webrtc_peer_connection_config())
            .await?;

        Ok(peer_connection)
    }

    pub fn make_client_token(&self) -> ClientToken {
        ClientToken::new(self.clients.clone())
    }
}

#[derive(Debug, Clone)]
pub struct ClientToken {
    acquired: sync::Shared<bool>,
    counter: sync::Shared<u32>,
}

impl ClientToken {
    pub fn new(counter: sync::Shared<u32>) -> Self {
        ClientToken {
            acquired: sync::share(false),
            counter,
        }
    }

    pub fn acquire(&self) {
        let mut acquired = self.acquired.lock().unwrap();
        if *acquired {
            return;
        }

        let mut counter = self.counter.lock().unwrap();

        *counter += 1;
        *acquired = true;
    }

    pub fn release(&self) {
        let mut acquired = self.acquired.lock().unwrap();
        if !*acquired {
            return;
        }

        let mut counter = self.counter.lock().unwrap();

        *counter -= 1;
        *acquired = false;
    }
}

#[derive(Debug, Clone)]
pub struct Config {
    pub endpoints: Endpoints,
    pub keep_local_addresses: bool,
    pub ephemeral_ports_range: Option<(u16, u16)>,
}

impl Config {
    pub fn make_webrtc_peer_connection_config(&self) -> WebRTCConfiguration {
        use webrtc::ice_transport::ice_server::RTCIceServer;

        let ice_servers: Vec<_> = self
            .endpoints
            .ice_servers
            .iter()
            .map(|url| RTCIceServer {
                urls: vec![url.clone()],
                ..Default::default()
            })
            .collect();

        WebRTCConfiguration {
            ice_servers,
            ..Default::default()
        }
    }
}

impl From<cli::Options> for Config {
    fn from(opts: cli::Options) -> Self {
        Self {
            endpoints: opts.endpoints.into(),
            keep_local_addresses: opts.keep_local_addresses,
            ephemeral_ports_range: opts.ephemeral_ports_range,
        }
    }
}

#[derive(Debug, Clone)]
pub struct Endpoints {
    pub broker_url: Url,
    pub nat_probe_url: Url,
    pub relay_url: Url,
    pub ice_servers: Vec<String>,
}

impl From<cli::Endpoints> for Endpoints {
    fn from(endpoints: cli::Endpoints) -> Self {
        Self {
            broker_url: endpoints.broker_url,
            nat_probe_url: endpoints.nat_probe_url,
            relay_url: endpoints.relay_url,
            ice_servers: endpoints.ice_servers,
        }
    }
}

pub async fn run(config: Config) -> Result<()> {
    use std::time::Duration;

    use tokio_graceful_shutdown::Toplevel;
    use tracing_futures::Instrument;

    tracing::info!("Starting server...");

    let http_client = reqwest::Client::builder()
        .user_agent(HTTP_USER_AGENT)
        .build()?;

    let proxy = SnowflakeProxy {
        config,
        nat_type: sync::share(nat::Type::Unknown),
        clients: sync::share(0),
        http_client,
    };

    Toplevel::new()
        .start(
            "connector",
            cloned!(
                proxy: subsystem!(|handle: Handle<_>| {
                    use futures::TryFutureExt;
                    use tokio_retry::strategy::ExponentialBackoff;
                    use tokio_retry::Retry;

                    let retry_strategy =
                        ExponentialBackoff::from_millis(10).max_delay(Duration::from_secs(5));

                    Retry::spawn(retry_strategy, || {
                        cloned!(handle, proxy:
                            session::connector(handle, proxy)
                                .instrument(tracing::trace_span!("connector"))
                                .inspect_err(|err| tracing::error!("Error on session connector: {:?}", err.report()))
                        )
                    })
                })
            ),
        )
        .start(
            "nat-type-updater",
            cloned!(
                proxy: subsystem!(|_handle: Handle<_>| {
                    use futures::TryFutureExt;
                    use tokio_retry::strategy::ExponentialBackoff;
                    use tokio_retry::Retry;

                    let retry_strategy =
                        ExponentialBackoff::from_millis(10).max_delay(Duration::from_secs(5));

                    Retry::spawn(retry_strategy, || {
                        cloned!(proxy:
                            nat::type_updater(proxy)
                                .instrument(tracing::trace_span!("nat.updater"))
                                .inspect_err(|err| tracing::error!("Error during NAT type checking: {:?}", err.report()))
                        )
                    })
                })
            ),
        )
        .catch_signals()
        .handle_shutdown_requests(Duration::from_secs(1))
        .await?;
    Ok(())
}
